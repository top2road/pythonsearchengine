# 파이썬을 이용한 검색 엔진 구현
- 작성자 : 김재민
- 개발자 : 김재민

<hr>

## 작업 환경
- OS : Mac M1 pro
- Python 3.8

## 아나콘다 설정
```angular2html
# 아나콘다 가상환경 리스트 조회
$ conda env list

# 아나콘다 가상환경 생성
$ conda create -n search_env python=3.8

# 아나콘다 가상환경 활성화
$ conda activate search_env

# 아나콘다 가상환경 비활성화
$ deactivate

# 아나콘다 가상환경 삭제
$ conda env remove -n search_env

# 파이썬 패키지 설치
$ pip install [PKG]
$ conda install [PATH] [PKG]
```

## Docker MySQL 컨테이너 배포
```angular2html
docker run -d -it \
--platform linux/amd64 \
--restart on-failure \
-p 3306:3306 \
-v /Users/jmkim/Documents/projects/mysql:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=P@ssw0rd \
--name mysql \
mysql:5.7
```

## MySQL 사용자 생성
```angular2html
CREATE USER 'jmkim'@'localhost' IDENTIFIED BY 'jmkim';
GRANT ALL PRIVILEGES ON *.* TO 'jmkim'@'localhost';
```

## MySQL Database 생성
```angular2html
CREATE DATABASE search_engine CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```

## MySQL 수집 후보 URL 테이블 생성
: MySQL 엔진 및 ROW_FORMAT 속성을 변경하여 url 컬럼에 UNIQUE 설정이 가능하도록 한다.
```angular2html
CREATE TABLE search_engine.candidate (
    id INT              NOT NULL AUTO_INCREMENT COMMENT '일련번호',
    url VARCHAR(1000)   NOT NULL COMMENT '수집할 웹페이지 주소',
    depth INT           NOT NULL COMMENT 'seed 사이트에서 상대적 깊이 정보',
    PRIMARY KEY CLUSTERED (id ASC),
    UNIQUE KEY url_uniq (url)
) ENGINE=InnoDB ROW_FORMAT=Dynamic DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
```

## MySQL 수집한 URL 테이블 생성
```angular2html
CREATE TABLE search_engine.web_page (
    web_id        INT               NOT NULL AUTO_INCREMENT,
    title         VARCHAR (255)     NOT NULL COMMENT '수집한 웹페이지 제목',
    url           VARCHAR (1000)    NOT NULL COMMENT '수집한 웹페이지 주소',
    description   TEXT              NOT NULL COMMENT '수집한 웹페이지 내용',
    mcnt          INT               NOT NULL COMMENT '수집한 웹페이지 단어수',
    PRIMARY KEY CLUSTERED (web_id ASC),
    UNIQUE KEY url_uniq (url)
) ENGINE=InnoDB ROW_FORMAT=Dynamic DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
```
