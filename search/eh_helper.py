# eh_helper.py

class EHHelper:
    @staticmethod
    def emit_tag_and_special_char(src):
        '''
        function : HTML TAG와 모든 특수문자 제거
        return:
        '''
        tamp = EHHelper.remove_tag(src)
        tamp = EHHelper.remove_html_special_char(tamp)
        tamp = EHHelper.remove_special_char(tamp)

        return tamp

    @staticmethod
    def remove_tag(src):
        '''
        function : HTML TAG 제거 함수
        return:
        '''

        while True:
            start, end = EHHelper.find_tag(src)
            if start is None or end is None:
                break
            # "<>"와 같이 들어왔을 때 if 실행
            if start < end:
                src = src[:start] + src[end+1:]
            # "><"와 같이 들어왔을 때 else 실행
            else:
                src = src[:end] + src[end+1:]

        return src

    @staticmethod
    def find_tag(src):
        '''
        function : HTML TAG의 시작 위치 끝 위치를 찾는 함수
        return:
        '''

        start = src.index("<") if "<" in src else None
        end = src.index(">") if ">" in src else None

        return start, end

    @staticmethod
    def remove_special_char(src):
        '''
         function : 일반 특수문자 제거 함수
        return:
        '''
        dest = ""
        for elem in src:
            # 알파벳이거나 공백이면 if 실행
            if str.isalpha(elem) or str.isspace(elem):
                dest += elem

        return dest

    @staticmethod
    def remove_html_special_char(src):
        '''
        function : HTML 전용 특수문자 제거 함수 -> ex) &quot;
        return:
        '''
        while True:
            start, end = EHHelper.find_html_special_char(src)
            if start is None or end is None:
                break
            # "<>"와 같이 들어왔을 때 if 실행
            if start < end:
                src = src[:start] + src[end+1:]
            # "><"와 같이 들어왔을 때 else 실행
            else:
                src = src[:end] + src[end+1:]

        return src

    @staticmethod
    def find_html_special_char(src):
        '''
        function : HTML 전용 특수문자 시작위치 끝 위치 찾는 함수
        return:
        '''
        start = src.index("&") if "&" in src else None
        end = src.index(";") if ";" in src else None


        return start, end

    @staticmethod
    def mysql_str_to_korea_str(src):
        '''
        function : MySQL DB 한글 인코딩
        return:
        '''
        try:
            src = src.encode("ISO-8859-1")
            src = src.decode("euc-kr")

        except:
            return ""

        else:
            return src