'''

'''
import requests as req
from bs4 import BeautifulSoup as bs


def get_url(url="""https://www.naver.com"""):
    response = req.get(url)
    if response.status_code == 200:
        return response
    else:
        raise "URL에 대한 반환 상태 코드가 200이 아닙니다."


# url = input("URL : ")
get_res = get_url()
html = bs(get_res.text, "html.parser")
print("웹페이지 제목 : {}".format(html.title.text))
print("웹페이지 내용 : {}".format(html.text))


